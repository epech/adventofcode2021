#pragma once
#include "Puzzle.h"
#include <vector>
class Puzzle_day03 :
    public Puzzle
{
public:
    Puzzle_day03(std::string filename);
    virtual void processInputLine(std::string line) override;
    virtual void processInputs() override;

private:
    void processPart1(void);
    void processPart2(void);
    uint64_t m_BitsLength;
    uint64_t m_Gamma;
    uint64_t m_Epsilon;
    std::vector<uint64_t> m_Diagnostics;
};

