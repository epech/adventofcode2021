#pragma once
#include <iostream>
#include <vector>

class Puzzle
{
public:
    Puzzle();
    virtual int loadInput(std::string inputFile);
    virtual void process();
    virtual void printOutput();
    virtual void printStats();

protected:
    virtual void processInputLine(std::string line) = 0;
    virtual void processInputs() = 0;
    void addResult(std::string result) { m_Output.push_back(result); }

private:
    std::vector<std::string> m_Output;
    uint64_t m_LoadInputTime;
    uint64_t m_ProcessTime;
};

