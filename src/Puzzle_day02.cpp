#include "Puzzle_day02.h"
#include <sstream>      // std::istringstream


Puzzle_day02::Puzzle_day02(std::string fileName)
{
	this->loadInput(fileName);
}

void Puzzle_day02::processInputLine(std::string line)
{
	// Decode the instruction
	std::istringstream ss(line);
	std::string strInstruction;
	std::string unit;
	ss >> strInstruction;
	ss >> unit;

	tInstruction instruction;
	if (strInstruction == "forward")
		instruction.direction = FORWARD;
	else if (strInstruction == "down")
		instruction.direction = DOWN;
	else if (strInstruction == "up")
		instruction.direction = UP;

	instruction.units = stoi(unit);

	m_Instructions.push_back(instruction);
}

void Puzzle_day02::processInputs()
{
	processPart1();
	processPart2();
}

void Puzzle_day02::processPart1()
{
	uint64_t horizontal{}, depth{};
	for (int i = 0; i < m_Instructions.size(); i++)
	{
		switch (m_Instructions[i].direction)
		{
		case FORWARD:
			horizontal += m_Instructions[i].units;
			break;
		case DOWN:
			depth += m_Instructions[i].units;
			break;
		case UP:
			depth -= m_Instructions[i].units;
			break;
		default:
			break;
		}
	}
	addResult(std::to_string(horizontal * depth));
}

void Puzzle_day02::processPart2()
{
	uint64_t horizontal{}, depth{}, aim{};
	for (int i = 0; i < m_Instructions.size(); i++)
	{
		switch (m_Instructions[i].direction)
		{
		case FORWARD:
			horizontal += m_Instructions[i].units;
			depth += aim * m_Instructions[i].units;
			break;
		case DOWN:
			aim += m_Instructions[i].units;
			break;
		case UP:
			aim -= m_Instructions[i].units;
			break;
		default:
			break;
		}
	}
	addResult(std::to_string(horizontal * depth));
}
