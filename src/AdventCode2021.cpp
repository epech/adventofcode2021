// AdventCode2021.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Puzzle_day01.h"
#include "Puzzle_day02.h"
#include "Puzzle_day03.h"

#include <chrono>
#include <vector>
using namespace std::chrono;

int main()
{
    std::vector<Puzzle*> puzzles;
    puzzles.push_back(new Puzzle_day01("input\\day01.txt"));
    puzzles.push_back(new Puzzle_day02("input\\day02.txt"));
    puzzles.push_back(new Puzzle_day03("input\\day03.txt"));

    for (Puzzle* puzzle : puzzles)
    {
        puzzle->process();
        puzzle->printOutput();
        puzzle->printStats();
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
