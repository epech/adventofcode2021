#pragma once
#include "Puzzle.h"
#include <vector>

class Puzzle_day02 :
    public Puzzle
{
public:
    Puzzle_day02(std::string fileName);
    // Inherited via Puzzle
    virtual void processInputLine(std::string line) override;
    virtual void processInputs() override;

private:
    enum eDirection { FORWARD, DOWN, UP, NUM_OF_DIRECTIONS };
    struct tInstruction {
        eDirection direction;
        int units;
    };
    std::vector<tInstruction> m_Instructions;
    void processPart1();
    void processPart2();
};

