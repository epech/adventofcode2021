#include "Puzzle.h"
#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

using namespace std;
using namespace std::chrono;

Puzzle::Puzzle()
    : m_LoadInputTime(0), m_ProcessTime(0)
{

}
int Puzzle::loadInput(std::string inputFile)
{
    auto start = high_resolution_clock::now();

    string line;
    ifstream myFile(inputFile);
    if (myFile.is_open())
    {
        while (getline(myFile, line))
        {
            processInputLine(line);
        }
        myFile.close();
    }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    this->m_LoadInputTime = duration.count();

    return 0;
}

void Puzzle::process()
{
    auto start = high_resolution_clock::now();
    // Start measuring
    processInputs();
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    this->m_ProcessTime = duration.count();
}

void Puzzle::printOutput()
{
    for (int i = 0; i < m_Output.size(); i++)
    {
        std::cout << "Part " << i + 1 << ":\t" << m_Output[i] << std::endl;
    }
}

void Puzzle::printStats()
{
    std::cout << "Inputs:\t" << m_LoadInputTime << "us\tProcess:\t" << m_ProcessTime << "us"<<std::endl;
}
