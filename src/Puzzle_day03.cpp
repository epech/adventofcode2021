#include "Puzzle_day03.h"
#include <string>

Puzzle_day03::Puzzle_day03(std::string filename):
	m_BitsLength(0),
	m_Gamma(0),
	m_Epsilon(0)
{
	this->loadInput(filename);
}

void Puzzle_day03::processInputLine(std::string line)
{
	if (m_BitsLength == 0)
	{
		m_BitsLength = line.size();
	}
	m_Diagnostics.push_back(stoi(line, 0, 2));
}

void Puzzle_day03::processInputs()
{
	processPart1();
}

void Puzzle_day03::processPart1(void)
{
	std::vector<uint64_t> bitsSet(m_BitsLength);
	int mid = m_Diagnostics.size() / 2;
	uint64_t mask{};

	for (int i = 0; i < m_BitsLength; i++)
	{
		for (int j = 0; j < m_Diagnostics.size(); j++)
		{
			if (m_Diagnostics[j] & (1ULL << i))
			{
				bitsSet[i]++;
			}
		}
	}
	// Build gamma
	for (int i = 0; i < bitsSet.size(); i++)
	{
		// Most common bit (1 or 0)
		if (bitsSet[i] > mid)
		{
			m_Gamma |= (1ULL << i);
		}
	}

	// Build mask
	for (int i = 0; i < m_BitsLength; i++)
	{
		mask |= (1ULL << i);
	}
	
	// Epsilon is just the negated of Gamma
	m_Epsilon = ((~m_Gamma) & mask);

	// Multiply before logging the result
	addResult(std::to_string(m_Gamma * m_Epsilon));
}

void Puzzle_day03::processPart2(void)
{
	bool found = false, bitSet = false;
	uint64_t i{}, bitPos{}, bitsSetCtr{};
	std::vector<uint64_t> oldNumbers;
	std::vector<uint64_t> newNumbers;

	oldNumbers = m_Diagnostics;

	while (!found)
	{

		bitsSetCtr = 0;
		bitSet = false;
		newNumbers.clear();
		// Get the most dominant bit
		for (int i = 0; i < oldNumbers.size(); i++)
		{
			if (oldNumbers[i] & (1ULL << bitPos))
			{
				bitsSetCtr++;
			}
		}

		if (bitsSetCtr >= (oldNumbers.size() / 2))
		{
			bitSet = true;
		}

		// Get rid of the numbers that don't meet the condition
		for (int i = 0; i < oldNumbers.size(); i++)
		{
			if (bitSet)
			{
				if (oldNumbers[i] & (1ULL << bitPos))
				{
					newNumbers.push_back(oldNumbers[i]);
				}
			}
			else
			{
				if ((oldNumbers[i] & (1ULL << bitPos)) == 0)
				{
					newNumbers.push_back(oldNumbers[i]);
				}
			}
		}
		bitPos++;

		if ((newNumbers.size() == 1))
		{

		}
	}
}
