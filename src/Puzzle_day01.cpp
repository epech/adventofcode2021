#include "Puzzle_day01.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
Puzzle_day01::Puzzle_day01()
{
}

Puzzle_day01::Puzzle_day01(std::string fileName)
{
    this->loadInput(fileName);
}


void Puzzle_day01::processInputLine(std::string line)
{
    m_Numbers.push_back(stoi(line));
}

void Puzzle_day01::processInputs()
{
    process_part1();
    process_part2();
}

void Puzzle_day01::process_part1()
{
    int counter{};
    // Does it increase?
    for (int i = 1; i < m_Numbers.size(); i++)
    {
        if (m_Numbers[i] > m_Numbers[i - 1])
            counter++;
    }

    addResult(to_string(counter));
}

void Puzzle_day01::process_part2()
{
    for (int i = 2; i < m_Numbers.size(); i++)
    {
        m_NumbersSlidingWindow.push_back(m_Numbers[i] + m_Numbers[i - 1] + m_Numbers[i - 2]);
    }

    // Check if it increases
    int counter{};
    // Does it increase?
    for (int i = 1; i < m_NumbersSlidingWindow.size(); i++)
    {
        if (m_NumbersSlidingWindow[i] > m_NumbersSlidingWindow[i - 1])
            counter++;
    }

    addResult(to_string(counter));
}
