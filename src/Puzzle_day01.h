#pragma once
#include "Puzzle.h"
#include <vector>
class Puzzle_day01 :
    public Puzzle
{
public:
    Puzzle_day01();
    Puzzle_day01(std::string fileName);
    virtual void processInputLine(std::string line);
    void processInputs() override;

private:
    void process_part1();
    void process_part2();
    std::vector<uint64_t> m_Numbers;
    std::vector<uint64_t> m_NumbersSlidingWindow;
};

